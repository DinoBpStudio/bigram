import java.util.Scanner;
import java.util.ArrayList;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;

/**
 * Created by rqfw47 on 11/14/17.
 */

public class test {
    public static void main(String args[]) {
        String inputURL = args[0];
        Boolean result = true;
        System.out.println("Input String: " + inputURL);
        //read data from file
        TextFileHelper textFileHelper = new TextFileHelper();
        String inputData = textFileHelper.parseTextFile(inputURL);

        //process data to get result
        BiGramReader biGramReader = new BiGramReader();
        ArrayList<BiGram> bigramArrayList = biGramReader.processInputString(inputData);

        if(bigramArrayList.size() == 0) {
            result = false;
        }

        //write result to file
        if(result) {
            result = textFileHelper.writeTextFile(biGramReader.getString(bigramArrayList), inputURL.replace(".txt", "") + "result.txt");
        }

        if(result) {
            System.out.println("Finish runing - successful");
        } else {
            System.out.println("Finish runing - failure - check your input");
        }

        if (args[1].equals("run-ut")) {
            UnittestHelper unittestHelper = new UnittestHelper();
            unittestHelper.runUT();
        }
    }
}
class TextFileHelper {
    public String parseTextFile(String url) {
        String retVal = "";
        String line = null;
        try {
            FileReader fileReader = new FileReader(url);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while((line = bufferedReader.readLine()) != null) {
                retVal += line;
            }
            bufferedReader.close();
        } catch(FileNotFoundException ex) {
            System.out.println("Unable to open file '" + url + "'");
        } catch(IOException ex) {
            ex.printStackTrace();
        }
        //remove all break line
        retVal.replaceAll("[\\t\\n\\r]+"," ");
        return retVal;
    }
    public boolean writeTextFile(String value, String fileName) {
        Boolean retVal = false;
        BufferedWriter bufferedWriter = null;
        FileWriter fileWriter = null;
        try {
            String content = value;
            fileWriter = new FileWriter(fileName);
            bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(content);
            retVal = true;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.close();
                }
                if (fileWriter != null) {
                    fileWriter.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return retVal;
    }
}
class BiGramReader {
    public  ArrayList<BiGram> processInputString(String input) {
        input = input.replaceAll("[\\t\\n\\r]+"," ");
        input = input.toLowerCase().trim();
        ArrayList<String> SingleGrams = new ArrayList<String>();
        Scanner scanner = new Scanner(input);
        String curWord = "";
        while (scanner.hasNext()) {
            curWord = scanner.next();
            SingleGrams.add(curWord);
        }

        ArrayList<BiGram> bigramList = new ArrayList<BiGram>();
        for (int i = 0; i < SingleGrams.size() - 1; i++) {
            String curBiGramString = SingleGrams.get(i) + " " + SingleGrams.get(i + 1);
            BiGram curBiGram = findBiGram(curBiGramString, bigramList);
            if(curBiGram == null) {
                bigramList.add(new BiGram(curBiGramString));
            } else {
                curBiGram.increase();
            }
        }
        return bigramList;
    }

    public BiGram findBiGram(String inputStr, ArrayList<BiGram> inputBigramArray) {
        BiGram foundBiGram = null;
        for (int i = 0; i < inputBigramArray.size(); i++) {
            BiGram tempBiGram = inputBigramArray.get(i);
            if(tempBiGram.getValue().equals(inputStr)) {
                foundBiGram = tempBiGram;
                break;
            }
        }
        return foundBiGram;
    }

    public void printStringArrayList(ArrayList<BiGram> input) {
        for(int i = 0; i < input.size(); i++) {
            System.out.println(input.get(i).getValue() + "----" + input.get(i).getCount());
        }
    }

    public String getString(ArrayList<BiGram> input) {
        String retVal = "";
        for(int i = 0; i < input.size(); i++) {
            retVal += input.get(i).getValue() + "----" + input.get(i).getCount() + "\n";
        }
        return retVal;
    }
}
class BiGram {
    BiGram(String value) {
        mCount = 1;
        mvalue = value;
    }

    public int getCount() {
        return mCount;
    }

    public void increase() {
        mCount++;
    }

    public String getValue() {
        return mvalue;
    }
    String mvalue;
    int mCount;
}

//Unitest
class UnittestHelper {
    public void runUT() {
        System.out.println("Start UT");
        try {
            Unittest unittest = new Unittest();
            Method[] methods = Unittest.class.getDeclaredMethods();
            for (Method method : methods) {
                if(method.getName().contains("test")) {
                    method.invoke(unittest);
                    System.out.println(method.getName() + " Passed");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        System.out.println("End UT");
    }
}
class Unittest {
    //test
    BiGramReader biGramReaderTest;
    ArrayList<BiGram> bigramArrayListTest;
    String inputString;
    //test data
    private final String testInputEmptyString  = "";
    private final String testInputOneWorString = "OneWord";
    private final String testInputNoRepeatBigramString = "one two three four five six seven eight nine ten";
    private final String testInputRepeatBigramString = "one one one one one one one one one one";
    private final String testInputMixBigramString = "one one two two one one two two one one";
    private final String testInputMixBigramString_1 = "11 22 33 33 22 11 00 00 00 44 22 11 33 11 33 33";
    private final String testInputMixUpperLowerCaseBigramString = "one ONE TWO two ONE one TWO TWO ONE one";
    private final String testInputMixUpperLowerCaseAndSpaceBigramString = "    one      ONE       TWO    two ONE   one TWO TWO ONE     one      ";
    private final String testInputMixUpperLowerCaseAndSpaceAndOtherBigramString = " one \t \t  \t  ONE \r \n      TWO    two ONE \n\t  one TWO TWO ONE     one      ";
    // assigning the values
    public void setUp(String input) {
        biGramReaderTest = new BiGramReader();
        inputString = input;
    }

    //test case
    public void testEmptyStringShouldReturnEmptyResult() {
        //set up
        setUp(testInputEmptyString);
        bigramArrayListTest = biGramReaderTest.processInputString(inputString);
        //validate result
        check(bigramArrayListTest.size() == 0);
    }
    public void testSingleWordStringShouldReturnEmptyResult() {
        //set up
        setUp(testInputOneWorString);
        bigramArrayListTest = biGramReaderTest.processInputString(inputString);
        //validate result
        check(bigramArrayListTest.size() == 0);
    }
    public void testInputNoRepeatBigramStringShouldReturnAllOne() {
        //set up
        setUp(testInputNoRepeatBigramString);
        bigramArrayListTest = biGramReaderTest.processInputString(inputString);
        //validate result
        check(bigramArrayListTest.size() == 9);
        for (BiGram biGram:bigramArrayListTest) {
            check(biGram.getCount() == 1);
        }
    }
    public void testInputRepeatBigramStringShouldReturnCorrectValue() {
        //set up
        setUp(testInputRepeatBigramString);
        bigramArrayListTest = biGramReaderTest.processInputString(inputString);
        //validate result
        checkEqual(1, bigramArrayListTest.size());
        for (BiGram biGram:bigramArrayListTest) {
            checkEqual(9, biGram.getCount());
            checkStrEqual("one one", biGram.getValue());
        }
    }
    public void testInputMixBigramStringShouldReturnCorrectValue() {
        //set up
        setUp(testInputMixBigramString);
        bigramArrayListTest = biGramReaderTest.processInputString(inputString);
        //validate result
        checkEqual(4, bigramArrayListTest.size());
        
        BiGram OneOne = biGramReaderTest.findBiGram("one one", bigramArrayListTest);
        BiGram TwoTwo = biGramReaderTest.findBiGram("two two", bigramArrayListTest);
        BiGram OneTwo = biGramReaderTest.findBiGram("one two", bigramArrayListTest);
        BiGram TwoOne = biGramReaderTest.findBiGram("two one", bigramArrayListTest);

        checkEqual(3, OneOne.getCount());
        checkEqual(2, TwoTwo.getCount());
        checkEqual(2, OneTwo.getCount());
        checkEqual(2, TwoOne.getCount());
    }
    public void testInputMixBigramStringShouldReturnCorrectValue_1() {
        //set up
        setUp(testInputMixBigramString_1);
        bigramArrayListTest = biGramReaderTest.processInputString(inputString);
        //validate result
        checkEqual(11, bigramArrayListTest.size());
        //"11 22 33 33 22 11 00 00 00 44 22 11 33 11 33 33"
        BiGram b1122 = biGramReaderTest.findBiGram("11 22", bigramArrayListTest);
        BiGram b2233 = biGramReaderTest.findBiGram("22 33", bigramArrayListTest);
        BiGram b3333 = biGramReaderTest.findBiGram("33 33", bigramArrayListTest);
        BiGram b3322 = biGramReaderTest.findBiGram("33 22", bigramArrayListTest);
        BiGram b4422 = biGramReaderTest.findBiGram("44 22", bigramArrayListTest);
        BiGram b1133 = biGramReaderTest.findBiGram("11 33", bigramArrayListTest);
        BiGram b0044 = biGramReaderTest.findBiGram("00 44", bigramArrayListTest);
        BiGram b0000 = biGramReaderTest.findBiGram("00 00", bigramArrayListTest);
        BiGram b2211 = biGramReaderTest.findBiGram("22 11", bigramArrayListTest);
        BiGram b1100 = biGramReaderTest.findBiGram("11 00", bigramArrayListTest);
        BiGram b3311 = biGramReaderTest.findBiGram("33 11", bigramArrayListTest);

        checkEqual(2, b3333.getCount());
        checkEqual(2, b0000.getCount());
        checkEqual(2, b2211.getCount());
        checkEqual(2, b1133.getCount());
        checkEqual(1, b0044.getCount());
        checkEqual(1, b1122.getCount());
        checkEqual(1, b2233.getCount());
        checkEqual(1, b3322.getCount());
        checkEqual(1, b4422.getCount());
        checkEqual(1, b1100.getCount());
    }

    public void testInputMixUpperLowerCaseBigramShouldReturnCorrectValue() {
        //set up
        setUp(testInputMixUpperLowerCaseBigramString);
        bigramArrayListTest = biGramReaderTest.processInputString(inputString);
        //validate result
        checkEqual(4, bigramArrayListTest.size());
        
        BiGram OneOne = biGramReaderTest.findBiGram("one one", bigramArrayListTest);
        BiGram TwoTwo = biGramReaderTest.findBiGram("two two", bigramArrayListTest);
        BiGram OneTwo = biGramReaderTest.findBiGram("one two", bigramArrayListTest);
        BiGram TwoOne = biGramReaderTest.findBiGram("two one", bigramArrayListTest);

        checkEqual(3, OneOne.getCount());
        checkEqual(2, TwoTwo.getCount());
        checkEqual(2, OneTwo.getCount());
        checkEqual(2, TwoOne.getCount());
    }

    public void testInputMixUpperLowerCaseAndSpaceBigramShouldReturnCorrectValue() {
        //set up
        setUp(testInputMixUpperLowerCaseAndSpaceBigramString);
        bigramArrayListTest = biGramReaderTest.processInputString(inputString);
        //validate result
        checkEqual(4, bigramArrayListTest.size());
        
        BiGram OneOne = biGramReaderTest.findBiGram("one one", bigramArrayListTest);
        BiGram TwoTwo = biGramReaderTest.findBiGram("two two", bigramArrayListTest);
        BiGram OneTwo = biGramReaderTest.findBiGram("one two", bigramArrayListTest);
        BiGram TwoOne = biGramReaderTest.findBiGram("two one", bigramArrayListTest);

        checkEqual(3, OneOne.getCount());
        checkEqual(2, TwoTwo.getCount());
        checkEqual(2, OneTwo.getCount());
        checkEqual(2, TwoOne.getCount());
    }

    public void testInputMixUpperLowerCaseAndSpaceAndOtherBigramShouldReturnCorrectValue() {
        //set up
        setUp(testInputMixUpperLowerCaseAndSpaceAndOtherBigramString);
        bigramArrayListTest = biGramReaderTest.processInputString(inputString);
        //validate result
        checkEqual(4, bigramArrayListTest.size());
        
        BiGram OneOne = biGramReaderTest.findBiGram("one one", bigramArrayListTest);
        BiGram TwoTwo = biGramReaderTest.findBiGram("two two", bigramArrayListTest);
        BiGram OneTwo = biGramReaderTest.findBiGram("one two", bigramArrayListTest);
        BiGram TwoOne = biGramReaderTest.findBiGram("two one", bigramArrayListTest);

        checkEqual(3, OneOne.getCount());
        checkEqual(2, TwoTwo.getCount());
        checkEqual(2, OneTwo.getCount());
        checkEqual(2, TwoOne.getCount());
    }
    //validate method
    public void checkStrEqual(String val1, String val2) {
        if (!val1.equals(val2)) {
            System.out.println("Expected value:" + val1 +"\nReal Value:" + val2);
            throw new RuntimeException("UT failed");
        }
    }
    public void checkEqual(int val1, int val2) {
        if (val1 != val2) {
            System.out.println("Expected value:" + val1 +"\nReal Value:" + val2);
            throw new RuntimeException("UT failed");
        }
    }
    public void check(Boolean input) {
        if (!input) {
            throw new RuntimeException("UT failed");
        }
    }
}