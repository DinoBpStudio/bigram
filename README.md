//Require:
java version "1.8.0_101"
Java(TM) SE Runtime Environment (build 1.8.0_101-b13)
Java HotSpot(TM) 64-Bit Server VM (build 25.101-b13, mixed mode)


//How to build

build
javac test.java

Run
java test <file-url> <run-ut>

file-url - location of your file
run-ut - option to run UT

//Output samples
OUTPUT FILE
The result will be export into a file same folder name is <intputfile>result.txt
when open will look like this

the quick----10
quick brown----5
brown fox----5
fox and----10
and fox----5
and the----5
quick blue----5
blue hare----5
hare the----4


ON SCREEN PRINTING
Input String: /Users/rqfw47/Desktop/Huy/testData.txt
Finish runing - successful
Start UT
testEmptyStringShouldReturnEmptyResult Passed
testSingleWordStringShouldReturnEmptyResult Passed
testInputNoRepeatBigramStringShouldReturnAllOne Passed
testInputRepeatBigramStringShouldReturnCorrectValue Passed
testInputMixBigramStringShouldReturnCorrectValue Passed
testInputMixBigramStringShouldReturnCorrectValue_1 Passed
testInputMixUpperLowerCaseBigramShouldReturnCorrectValue Passed
testInputMixUpperLowerCaseAndSpaceBigramShouldReturnCorrectValue Passed
testInputMixUpperLowerCaseAndSpaceAndOtherBigramShouldReturnCorrectValue Passed
End UT
